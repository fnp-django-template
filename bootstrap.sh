#
# Use as: 
#
#   source <(curl https://py.mdrn.pl/django)
#


# Make it a function, so that it works with `source`
start_project() {

local DJANGO_REQ='Django>=1.8,<1.9'
local DJANGO_ROOT='src'
local PYPI='https://py.mdrn.pl:8443/simple'
local DEFAULT_PYTHON="`which python3.4`"

if [ -z "$DEFAULT_PYTHON" ]; then
    DEFAULT_PYTHON="`which python3`"
fi
if [ -z "$DEFAULT_PYTHON" ]; then
    DEFAULT_PYTHON="`which python`"
fi

local PROJECT=$PROJECT
if [ -z "$PROJECT" ]; then
    PROJECT="$1"
fi

if [ -z "$FNP_PROJECT_TEMPLATE" ]; then
    local FNP_PROJECT_TEMPLATE='https://git.mdrn.pl/fnp-django-template.git/snapshot/HEAD.tar.gz'
fi

local VIRTUALENVWRAPPER_PATHS="
    /etc/bash_completion.d/virtualenvwrapper
    /usr/bin/virtualenvwrapper.sh
    /usr/local/bin/virtualenvwrapper.sh
"

# Colorful output.
local strong='\e[0;32m'
local error='\e[1;31m'
local normal='\e[0m'

local PYTHON="$PYTHON"
if [ -z "$PYTHON" ]; then
    echo "Defaulting to $DEFAULT_PYTHON."
    PYTHON="$DEFAULT_PYTHON"
fi


echo "Create new Django project."
while [ -z "$PROJECT" ]
do
    echo "Name of the project:"
    read PROJECT
done
echo -e "Project: ${strong}${PROJECT}${normal}"

if ! type mkvirtualenv > /dev/null; then
    for venv in $VIRTUALENVWRAPPER_PATHS
    do
        if [ -e "$venv" ]
        then
            local VIRTUALENVWRAPPER="$venv"
            break
        fi
    done
    if [ "$VIRTUALENVWRAPPER" ]
    then
        echo "virtualenvwrapper found at $VIRTUALENVWRAPPER."
        source "$VIRTUALENVWRAPPER"
    else
        echo -e "${error}ERROR: virtualenvwrapper not found. Tried locations:${normal}"
        echo "$VIRTUALENVWRAPPER_PATHS"
        echo -e "${error}Install virtualenvwrapper or add the correct path to this script.${normal}"
        echo "Aborting."
        return
    fi
fi

echo -e "${strong}Creating virtualenv: $PROJECT $PYTHON...${normal}"
mkvirtualenv "$PROJECT" --python "$PYTHON"
echo -e "${strong}Upgrading pip...${normal}"
pip install -i "$PYPI" -U pip
echo -e "${strong}Installing Django...${normal}"
pip install -i "$PYPI" "$DJANGO_REQ"
pip install -i "$PYPI" --pre django-startproject-plus

echo -e "${strong}Downloading project template...${normal}"
if [[ "$FNP_PROJECT_TEMPLATE" =~ ^(https?|ftp):// ]]; then
    local PROJECT_TMPDIR=`mktemp -d fnp-django-template-XXX`
    curl "$FNP_PROJECT_TEMPLATE" | tar xzf - -C "$PROJECT_TMPDIR"
    local TEMPLATE=("$PROJECT_TMPDIR"/fnp-django-template-*/src)
else
    local TEMPLATE="$FNP_PROJECT_TEMPLATE"
fi

echo -e "${strong}Starting the project...${normal}"
django-startproject.py \
    --template "$TEMPLATE" \
    --name NOTICE \
    --extra_context='{"year": "`date +%Y`"}' \
    "$PROJECT"

if [ -n "$PROJECT_TMPDIR" ]; then
    rm -rf "$PROJECT_TMPDIR"
fi

cd "$PROJECT"

chmod +x "$DJANGO_ROOT"/manage.py
mv _gitignore .gitignore
cp etc/local_settings.py.sample etc/local_settings.py
cp etc/local_settings_test.py.sample etc/local_settings_test.py
ln -s ../../../etc/local_settings.py ../../../etc/local_settings_test.py $DJANGO_ROOT/$PROJECT/settings/

echo -e "${strong}Installing requirements...${normal}"
pip install -i "$PYPI" -r requirements/dev.txt
echo -e "${strong}Running migrate...${normal}"
"$DJANGO_ROOT"/manage.py migrate #--noinput

echo -e "${strong}Starting new git repository...${normal}"
git init

echo -e "${strong}What next?${normal}"
echo " * Work on your app, commit to git."
echo " * Review fabfile, use fab for deployment."


}
start_project

# The following is just for displaying it as a webpage:<!--
#--><style>body{white-space:pre;color:#ddd}</style><h1 style="color:#000;text-align:center;position:fixed;top:0;bottom:0;left:0;right:0;white-space:normal">source &lt;(curl <span id="location"></span>)</h1><script>document.getElementById('location').innerHTML=window.location;</script>

