from django.conf.urls import include, url
from django.conf import settings
from django.contrib import admin
import django_cas_ng.views

urlpatterns = [
    # Examples:
    # url(r'^$', {{ project_name }}.views.home, name='home'),
    # url(r'^{{ project_name }}/', include('foo.urls')),

    # Admin stuff.
    url(r'^admin/', include(admin.site.urls)),

    # CAS stuff.
    url(r'^accounts/login/$', django_cas_ng.views.login),
    url(r'^accounts/logout/$', django_cas_ng.views.logout),
]

# Media in DEBUG mode
if settings.DEBUG:
    from django.views.static import serve
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT,
            'show_indexes': True,
        }),
   ]

