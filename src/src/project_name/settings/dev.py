from .base import *

_DEBUG_TOOLBAR = False

ADMINS = MANAGERS = [
    ('Admin', 'admin@example.com'),
]

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': path.join(PROJECT_DIR, 'var', 'db.sqlite3'),
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
INTERNAL_IPS = ['127.0.0.1']
SECRET_KEY = 'secret_key'

for _t in TEMPLATES:
    _t['OPTIONS']['debug'] = DEBUG
    _t['OPTIONS']['context_processors'].append('django.template.context_processors.debug')

if _DEBUG_TOOLBAR:
    INSTALLED_APPS += ('debug_toolbar',)
    MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)

