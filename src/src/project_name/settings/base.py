# -*- coding: utf-8 -*-
# Base settings for {{ project_name }} project.
from __future__ import unicode_literals

from os import path

PROJECT_DIR = path.dirname(path.dirname(path.dirname(path.abspath(path.dirname(__file__)))))


# =============
#  DJANGO CORE
# =============

INSTALLED_APPS = (
    '{{ project_name }}',
    'fnpdjango',
    'pipeline',
    # Disable, if not using Piwik.
    'piwik',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.messages',
    'django.contrib.sessions',
    'django.contrib.staticfiles',
)

LANGUAGE_CODE = 'pl'

LANGUAGES = (
    ('pl', u'polski'),
#    ('en', u'English'),
)

MEDIA_ROOT = path.join(PROJECT_DIR, 'var/media/')
MEDIA_URL = '/media/'

MIDDLEWARE_CLASSES = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    #'django.middleware.locale.LocaleMiddleware',
    'fnpdjango.middleware.URLLocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django_cas_ng.middleware.CASMiddleware', # Remove if not using CAS.
    'django.contrib.messages.middleware.MessageMiddleware',
    'fnpdjango.middleware.SetRemoteAddrFromXRealIP'
]

ROOT_URLCONF = '{{ project_name }}.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
            ],
        },
    },
]


TIME_ZONE = 'Europe/Warsaw'
USE_I18N = True
USE_L10N = True
USE_TZ = True
WSGI_APPLICATION = '{{ project_name }}.wsgi.application'


# =============
#  DJANGO AUTH
# =============

# Remove if not using CAS.
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'django_cas_ng.backends.CASBackend',
)

# ===============
#  DJANGO STATIC
# ===============

STATIC_ROOT = path.join(PROJECT_DIR, 'var/static/')
STATIC_URL = '/static/'
STATICFILES_STORAGE = 'fnpdjango.pipeline_storage.GzipPipelineCachedStorage'

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'pipeline.finders.PipelineFinder',
]


# ========
#  CAS_NG
# ========

CAS_SERVER_URL = 'https://logowanie.nowoczesnapolska.org.pl/cas/'
CAS_VERSION = '3'


# ==========
#  PIPELINE
# ==========

PIPELINE_CSS = {
    'base': {
        'source_filenames': (
          'css/base.scss',
        ),
        'output_filename': 'compressed/base.css',
    },
}

PIPELINE_JS = {
    'base': {
        'source_filenames': (
            'jquery.min.js',
        ),
        'output_filename': 'compressed/base.js',
    },
}

PIPELINE_CSS_COMPRESSOR = None
PIPELINE_JS_COMPRESSOR = None

PIPELINE_COMPILERS = (
    'pipeline.compilers.sass.SASSCompiler',
)
