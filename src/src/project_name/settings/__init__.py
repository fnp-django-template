# -*- coding: utf-8 -*-
# Settings loader for {{ project_name }} project.
from __future__ import unicode_literals

import sys


if len(sys.argv) > 1 and sys.argv[1] == 'test':
    try:
        from .local_settings_test import *
    except ImportError:
        from os import path
        local_settings_path = path.dirname(__file__) + '/local_settings_test.py'
        if not path.exists(local_settings_path):
            # No local test settings.
            from .test import *
        else:
            # Error in local test settings - propagate.
            raise
else:
    try:
        from .local_settings import *
    except ImportError:
        from os import path
        local_settings_path = path.dirname(__file__) + '/local_settings.py'
        if not path.exists(local_settings_path):
            # No local settings file.
            # Assuming dev mode, because the file would've been created during deployment.
            from warnings import warn
            warn("File '%s' does not exist. Loading dev settings as a fallback." % local_settings_path)
            from .dev import *
        else:
            # Error in local settings - propagate.
            raise
