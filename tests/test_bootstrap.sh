#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# Colorful output.
strong='\e[0;32m'
error='\e[1;31m'
normal='\e[0m'

tmpdir="`mktemp -d "$DIR/test-XXX"`"
echo "$tmpdir"


export WORKON_HOME="$tmpdir/v"
mkdir "$WORKON_HOME"


PYTHON="$1"
if [ -z "$PYTHON" ]; then
  PYTHON=python
fi
PYTHON_VER="`$PYTHON -V 2>&1`"
echo -e "${strong}Using $PYTHON_VER${normal}"
PYTHON="`which $PYTHON`"

FNP_PROJECT_TEMPLATE="$DIR"/../src
projectdir="$tmpdir"/p
mkdir "$projectdir"
cd "$projectdir"

PROJECT=TEST_PROJECT

. "$DIR"/../bootstrap.sh


# Test for active virtualenv
if [ "$VIRTUAL_ENV" != "$WORKON_HOME/TEST_PROJECT" ]; then
    echo -e "${error}Expected to be in virtualenv, found python: `which python`${normal}"
fi
if [ "`python -V 2>&1`" != "$PYTHON_VER" ]; then
    echo -e "${error}Expected $PYTHON_VER, found `python -V 2>&1`${normal}"
fi

# Test that there is only project dir in the dir.
if [ "`ls "$projectdir"`" != "TEST_PROJECT" ]; then
    echo -e "${error}Only created project expected in project dir, found: `ls "$projectdir"`${normal}"
fi

rm -rf "$tmpdir"
